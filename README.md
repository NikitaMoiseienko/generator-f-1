# generator-f-1 [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage percentage][coveralls-image]][coveralls-url]
> 

## Installation

First, install [Yeoman](http://yeoman.io) and generator-f-1 using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-f-1
```

Then generate your new project:

```bash
yo f-1
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

MIT © [nik]()


[npm-image]: https://badge.fury.io/js/generator-f-1.svg
[npm-url]: https://npmjs.org/package/generator-f-1
[travis-image]: https://travis-ci.com//generator-f-1.svg?branch=master
[travis-url]: https://travis-ci.com//generator-f-1
[daviddm-image]: https://david-dm.org//generator-f-1.svg?theme=shields.io
[daviddm-url]: https://david-dm.org//generator-f-1
[coveralls-image]: https://coveralls.io/repos//generator-f-1/badge.svg
[coveralls-url]: https://coveralls.io/r//generator-f-1

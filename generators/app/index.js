"use strict";
const Generator = require("yeoman-generator");
const chalk = require("chalk");
const yosay = require("yosay");
const path = require("path");

module.exports = class extends Generator {
  prompting() {
    // Have Yeoman greet the user.
    this.log(
      yosay(
        `Welcome to the praiseworthy ${chalk.red("generator-f-1")} generator!`
      )
    );

    const prompts = [
      {
        type: "input",
        name: "path",
        message:
          "Your root folder's name (possibly with path to it from current dir): ",
        default: "f1-project"
      }
    ];

    return this.prompt(prompts).then(props => {
      // To access props later use this.props.someAnswer;
      this.props = props;
    });
  }

  writing() {
    this.fs.copy(
      this.templatePath(""),
      this.destinationPath(path.join(__dirname, this.props.path), "")
    );
  }

  install() {
    this.installDependencies();
  }
};
